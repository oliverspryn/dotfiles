# Dotfiles

This is [Oliver Spryn's](https://oliverspryn.com/) constantly evolving dotfile setup repository. My terminal is not amazing, but it works well for me. Interested in giving it a shot? Try out these setup directions and you should be good to go. Keep in mind that these steps (largely) work for Linux, WSL, and Mac-based systems, with minor variances, such as paths to things inside of the `.zshrc` script.

## Setting up Git

Installing these setup scripts is simple:

1. Copy the entire contents of the `./git` folder to your home (`~`) directory. Do not copy the `.git` folder directly to your `~` folder, as that will not work, by convention.
1. Close and restart the terminal

## Setting up the Terminal

Here are the necessary steps to setting up my terminal:

1. Install Powerlevel10k (a MUST \*_\*)
1. Install SSH key
1. Add GPG keys to key ring
1. Install GPG2 (if Git does not work with GPG, common on Ubuntu)

### Windows 11 &amp; WSL

You will need to do these things if you are running a Ubuntu terminal on WSL:

1. Install the [WSL2 Linux kernal update package](https://docs.microsoft.com/en-us/windows/wsl/install-manual#step-4---download-the-linux-kernel-update-package)
1. Unmount the C:/ with `sudo umount /mnt/c`
1. Remount the C:/ with `sudo mount -t drvfs C: /mnt/c -o metadata`

The [last two steps are necessary](https://askubuntu.com/a/1118158) for Git to be able to write to the host OS.

### Powerlevel10k

This program is a theme for zsh-based shells. It's project is available here: [https://github.com/romkatv/powerlevel10k](https://github.com/romkatv/powerlevel10k).

If not already using zsh, as the default shell:

1. Run: `sudo apt-get update && sudo apt-get install zsh -y`
1. Change to zsh with: `chsh -s /usr/bin/zsh` **Note:** Do NOT run with `sudo` or this will change the default shell for the root user.
1. Restart your terminal
1. Verify that zsh is the current shell with: `echo $SHELL`

To install the theme:

1. Install [Meslo Nerd Font](https://github.com/romkatv/powerlevel10k#meslo-nerd-font-patched-for-powerlevel10k) to your operating system, and tell your terminal to use that as the font. This depends on the OS and type of terminal you are using, so look at the linked Powerlevel10k documentation.
1. Clone the repository, like so: `git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/powerlevel10k`
1. Install the theme into your `.zshrc` file, like so: `echo 'source ~/powerlevel10k/powerlevel10k.zsh-theme' >> ~/.zshrc`
    - Already done inside of the `.zshrc` file in this repository. You may want to clear out that Manual configuration comment to have Powerlevel10k do it for you with a clean slate.
1. Restart your shell
1. Run the setup script with `p10k configure` (should run on first launch anyway)

### Install SSH Key

I have my SSH key stored in an encrypted network drive. To setup the entire process, including passphrase management, just follow these steps:

1. Get the pre-existing SSH keys from the encrypted network drive
1. Move those files to `~/.ssh`
1. Move the contents of `./ssh` folder to `~/.ssh` on your machine
1. Set the proper permissions on the private key with `chmod 400 ~/.ssh/id_rsa`
1. Add `eval $(ssh-agent) > /dev/null 2>&1` to `~/.zshrc`. The `/dev/null` redirect is to prevent the process from announcing its process ID once it starts.
    - Already done inside of the `.zshrc` file in this repository

### Install GPG Keys

I have a full-featured article here on how to make [Git and GPG work together](https://betterprogramming.pub/a-handbook-to-gpg-and-git-5990f8db4361). Since I'm not setting up Git here, I'll just mention the GPG-portion of the setup. Like the SSH keys, my GPG keys also live in an encrypted network drive.

1. Get the pre-existing GPG keys from the encrypted network drive
1. Run `gpg --import the-public-key.asc` and `gpg --import the-private-key.asc` to add both parts to your key ring
1. Run `gpg --list-secret-keys --keyid-format LONG` to get the key ID of the newly-imported key
1. Grant ultimate trust to that key with `gpg --edit-key <key-ID>`, type `trust`, then `5`, and `quit`.

### Install GPG2

For some reason, GPG does not play nicely with Git on Ubuntu-based systems, but GPG2 does. If you run into issues with signing commits with GPG and Git, install GPG2, instead. More information on this is available in the [above-mentioned article](https://betterprogramming.pub/a-handbook-to-gpg-and-git-5990f8db4361).

1. Run: `sudo apt-get update && sudo apt-get install gnupg2 -y`
1. Update Git to use GPG2 instead of GPG: `git config --global gpg.program gpg2`
    - Already done inside of the `.gitconfig` file in this repository
1. Add this somewhere inside of your `~/.zshrc` file: `export GPG_TTY=$(tty)`
    - Already done inside of the `.zshrc` file in this repository

### Configure ZSH

I have a small `.zshrc` file. The file is probably best to copy and paste manually, since Powerlevel10k adds in its own custom setup at the top.

1. Copy the contents of `./zsh/.zshrc` that lie below the Manual Configuration comment. Everything above that line is automatically added by Powerlevel10k and may be custom to your setup.
1. Paste your clipboard contents to the bottom of your `~/.zshrc` file
1. Reload your terminal
