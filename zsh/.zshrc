source ~/powerlevel10k/powerlevel10k.zsh-theme

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

#######################################
# Manual configuration from here down #
#######################################

# Load Git completion
zstyle ':completion:*:*:git:*' script ~/.zsh/completion.bash
fpath=(~/.zsh $fpath)

autoload -Uz compinit && compinit

# Enable GPG2
# Only use on Ubuntu-based systems which do not work with GPG and require GPG2
export GPG_TTY=$(tty)

# Load the SSH Agent for SSH passphrase storage
eval $(ssh-agent) > /dev/null 2>&1

# Custom aliases
alias cd..='cd ..'
alias cls='clear'
alias l='ls'
alias ll='ls -lash'
alias md='mkdir'

# Android Development Environment modifications
# For Mac workstations
#export ANDROID_HOME=$HOME/Library/Android/sdk
#export JDK_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_281.jdk/Contents/Home/
#export PATH=$PATH:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools
