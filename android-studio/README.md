# Windows Setup
It is helpful to disable Windows Defender support to speed up Android Studio's performance and compilation. Here is my current list of excluded folders:

- `%USERPROFILE%\.android`
- `%USERPROFILE%\.AndroidStudioX.Y`
- `%USERPROFILE%\.gradle`
- `%USERPROFILE%\.m2`
- `%LOCALAPPDATA%\Android\Sdk`
- `%LOCALAPPDATA%\JetBrains\IntelliJIdeaXXXX.Y`
- `%LOCALAPPDATA%\JetBrains\Toolbox\apps` (Only when Android Studio is installed via the JetBrains Toolbox)
- `<project directory>`
